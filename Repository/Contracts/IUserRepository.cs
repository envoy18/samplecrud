﻿using SampleCRUD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleCRUD.Repository.Contracts
{
    public interface IUserRepository 
    {
        Task<int> AddUser(User user);

        int AddAllUser(List<User> users);

        List<User> GetAllUsers();

        Task<int> UpdateUser(User user);

        User GetUserById(Guid id);

        Task<int> DeleteUserById(Guid id);

        User GetByUserFirstName(List<User> userList,string firstName);

        void UserMethod(User user);

        void UserMethod(Guid id);
    }
}
