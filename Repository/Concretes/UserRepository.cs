﻿using SampleCRUD.Model;
using SampleCRUD.Model.DBContext;
using SampleCRUD.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleCRUD.Repository.Concretes
{
    public class UserRepository : IUserRepository
    {
        private ELearningDbContext _context;

        public UserRepository(ELearningDbContext dbContext)
        {
            _context = dbContext;
        }

        public int AddAllUser(List<User> users)
        {
             _context.TblUsers.AddRangeAsync(users);
            return _context.SaveChanges();
        }

        public Task<int> AddUser(User user)
        {
            throw new NotImplementedException();
        }

        public int CreateUser(User user)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteUserById(Guid id)
        {
            throw new NotImplementedException();
        }

        public List<User> GetAllUsers()
        {
            return _context.TblUsers.ToList();
        }

        public User GetByUserFirstName(List<User> userList, string firstName)
        {
            return userList.Where(x => x.FirstName == firstName).FirstOrDefault();
        }

        public User GetUserById(Guid id)
        {
            return _context.TblUsers.Where(lamda => lamda.Id == id).FirstOrDefault();
        }

        public Task<int> UpdateUser(User user)
        {
            throw new NotImplementedException();
        }

        public void UserMethod(User user)
        {
            throw new NotImplementedException();
        }

        public void UserMethod(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
