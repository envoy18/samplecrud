﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleCRUD.Model
{
    public class Address : BaseClass
    {
        public Guid UserId { get; set; }

        private string StreetName { get; set; }

        public string Municipality { get; set; }

        public short StreetNumber { get; set; }

        public int ZipCode { get; set; }

        static long Coords = 12678;

        public void SetStreetName(string streetName)
        {
            StreetName = streetName;
        }

        
    }
}
