﻿using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleCRUD.Model.DBContext
{
    public class ELearningDbContext : DbContext
    {
        public ELearningDbContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<User> TblUsers { get; set; }
    }
}
