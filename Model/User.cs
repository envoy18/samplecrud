﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleCRUD.Model
{
    public class User : BaseClass
    {
        public User()
        {
        }

        public User(string firstName, 
                    string middleName, 
                    string lastName, 
                    DateTime birthday, 
                    int age, 
                    int gender, 
                    string email)
        {
            FirstName = firstName;
            MiddleName = middleName;
            LastName = lastName;
            Birthday = birthday;
            Age = age;
            Gender = gender;
            Email = email;
        }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public DateTime Birthday { get; set; }

        public int Age { get; set; }

        public int Gender { get; set; }

        public string Email { get; set; }

    }
}
