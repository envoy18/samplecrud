﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleCRUD.Model
{
    public class Item : BaseClass
    {
        public Item()
        {
        }

        public Item(string itemName, string itemDescription)
        {
            ItemName = itemName;
            ItemDescription = itemDescription;
            Stock = ReStock();
        }

        public string ItemName { get; set; }

        public string ItemDescription { get; set; }

        private int Stock { get; set; }

        public void SetStock(int newStock)
        {
            Stock = newStock;
        }

        private int ReStock()
        {
            return 100;
        }
    }
}
