﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleCRUD.Model
{
    public class BaseClass
    {
        public Guid Id { get; set; }

        public Guid UserCreated { get; set; }

        public DateTime DateCreated { get; set; }

        public Guid UserUpdated { get; set; }

        public DateTime DateUpdated { get; set; }
    }
}
