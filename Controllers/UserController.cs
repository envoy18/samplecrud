﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SampleCRUD.Model;
using SampleCRUD.Repository.Concretes;
using SampleCRUD.Repository.Contracts;

namespace SampleCRUD.Controllers
{
    [Produces("application/json")]
    [Route("api/User")]
    public class UserController : Controller
    {
        private readonly IUserRepository _userRepository;
        public Address UserAddress;

        public UserController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [Route("GetAllUser")]
        [HttpGet]
        public IActionResult GetAllUser()
        {
            List<User> userList = _userRepository.GetAllUsers();

            return Ok(userList);
        }

        [Route("GetUserById")]
        [HttpGet]
        public IActionResult GetUserById(Guid id)
        {
            var newGuid = Guid.NewGuid();
            User user = _userRepository.GetUserById(id);

            return Ok(user);
        }
        
    }

    public class SampleData
    {
        public string id { get; set; }
    }
}